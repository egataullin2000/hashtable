import data.Data;
import data.DataGenerator;
import data.DataProvider;
import hashtable.HashTable;
import model.Player;

import java.util.List;
import java.util.UUID;

public class Main {

    private static List<UUID> keys;
    private static HashTable<UUID, Player> dataset;

    public static void main(String[] args) {
        Data[] dataArray = DataProvider.read();
        for (Data data : dataArray) {
            System.out.println("Testing data of " + data.size() + " elements");
            keys = data.getKeys();
            dataset = data.getDataset();
            removeItem();
            removeItem();
            putItem();
            putItem();
            findItem();
            findItem();
        }
    }

    private static void removeItem() {
        UUID key = keys.get(0);
        long startTime = System.nanoTime();
        dataset.remove(key);
        notifyItemWas("removed", System.nanoTime() - startTime);
        keys.remove(0);
    }

    private static void notifyItemWas(String what, long time) {
        System.out.println("\tItem was " + what + " in " + time + " ns");
    }

    private static void putItem() {
        UUID key = UUID.randomUUID();
        Player player = DataGenerator.randomPlayer();
        keys.add(key);
        long startTime = System.nanoTime();
        dataset.put(key, player);
        notifyItemWas("put", System.nanoTime() - startTime);
    }

    private static void findItem() {
        UUID key = keys.get(0);
        long startTime = System.nanoTime();
        dataset.get(key);
        notifyItemWas("found", System.nanoTime() - startTime);
    }

}
