package data;

import hashtable.HashTable;
import model.Player;

import java.util.List;
import java.util.UUID;

public class Data {

    private List<UUID> keys;
    private HashTable<UUID, Player> dataset;

    Data(List<UUID> keys, HashTable<UUID, Player> dataset) {
        this.keys = keys;
        this.dataset = dataset;
    }

    public List<UUID> getKeys() {
        return keys;
    }

    public HashTable<UUID, Player> getDataset() {
        return dataset;
    }

    public int size() {
        return keys.size();
    }

}
