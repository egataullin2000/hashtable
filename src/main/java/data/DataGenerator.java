package data;

import hashtable.HashTable;
import model.Player;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

public class DataGenerator {

    public static void main(String[] args) {
        DataProvider.write(generate());
    }

    public static Data[] generate() {
        var dataCount = 50 + new Random().nextInt(50);
        var dataArray = new Data[dataCount];
        for (int i = 0; i < dataCount; i++) {
            var length = 100 + new Random().nextInt(9_900);
            var keys = new ArrayList<UUID>(length);
            var dataset = new HashTable<UUID, Player>(length);
            for (int j = 0; j < length; j++) {
                keys.add(UUID.randomUUID());
                dataset.put(keys.get(j), randomPlayer());
            }
            dataArray[i] = new Data(keys, dataset);
        }

        return dataArray;
    }

    public static Player randomPlayer() {
        return new Player(randomNickname(), randomLevel());
    }

    private static String randomNickname() {
        var stringBuilder = new StringBuilder();
        var nicknameLength = 10 + new Random().nextInt(11);
        for (int i = 0; i < nicknameLength; i++) {
            stringBuilder.append(randomSymbol());
        }

        return stringBuilder.toString();
    }

    private static char randomSymbol() {
        return (char) (65 + new Random().nextInt(26));
    }

    private static int randomLevel() {
        return new Random().nextInt(100);
    }

}
