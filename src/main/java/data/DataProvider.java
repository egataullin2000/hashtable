package data;

import hashtable.HashTable;
import model.Player;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

public class DataProvider {

    private static final String SOURCE = "data.txt";

    public static void write(Data[] dataArray) {
        try {
            var writer = new FileWriter(SOURCE);
            writer.write(Integer.toString(dataArray.length));
            writer.write("\n");
            for (var data : dataArray) {
                writer.write(Integer.toString(data.size()));
                writer.write("\n");
                data.getDataset().forEach(player -> {
                    try {
                        writer.write(player.toString());
                        writer.write("\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Data[] read() {
        Data[] dataArray = null;
        try {
            var scanner = new Scanner(new FileReader(SOURCE));
            dataArray = new Data[scanner.nextInt()];
            for (int i = 0; i < dataArray.length; i++) {
                var dataSize = scanner.nextInt();
                var keys = new ArrayList<UUID>(dataSize);
                var dataset = new HashTable<UUID, Player>();
                for (int j = 0; j < dataSize; j++) {
                    var key = UUID.randomUUID();
                    keys.add(key);
                    var player = new Player(scanner.next(), scanner.nextInt());
                    dataset.put(key, player);
                }
                dataArray[i] = new Data(keys, dataset);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return dataArray;
    }

}
