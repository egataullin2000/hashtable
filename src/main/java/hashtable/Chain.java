package hashtable;

import java.util.function.Consumer;

class Chain<K, V> {

    private Element first, last;

    void add(K key, V value) {
        var current = first;
        while (current != last) {
            if (key.equals(current.key)) {
                current.value = value;
                return;
            }
            current = current.next;
        }
        addUniq(new Element(key, value));
    }

    /**
     * Use this function only when you are sure that element with your element.key is not presented yet.
     */
    void addUniq(Element element) {
        if (first == null) {
            first = last = element;
        } else {
            last.next = element;
            element.prev = last;
            last = element;
        }
    }

    V get(K key) {
        var element = getElement(key);
        return element == null ? null : element.value;
    }

    private Element getElement(K key) {
        if (first == null) return null;

        var current = first;
        while (current != last) {
            if (key.equals(current.key)) {
                return current;
            }
            current = current.next;
        }

        return key.equals(current.key) ? current : null;
    }

    boolean remove(K key) {
        Element element = getElement(key);
        if (element == null) return false;
        if (element == first) first = first.next;
        if (element == last) last = last.prev;
        if (element.hasPrev()) element.prev.next = element.next;
        if (element.hasNext()) element.next.prev = element.prev;

        return true;
    }

    boolean isEmpty() {
        return first == null;
    }

    void forEachElement(Consumer<Element> action) {
        if (first == null) return;

        for (Element element = first; element != last; element = element.next) {
            action.accept(element);
        }
        action.accept(last);
    }

    void forEach(Consumer<V> action) {
        forEachElement(element -> action.accept(element.value));
    }

    @Override
    public String toString() {
        if (first == null) return "";

        var stringBuilder = new StringBuilder();
        forEachElement(element -> {
            stringBuilder.append(element);
            if (element != last) stringBuilder.append(", ");
        });

        return stringBuilder.toString();
    }

    class Element {
        K key;
        private V value;
        private Element prev, next;

        private Element(K key, V value) {
            this.key = key;
            this.value = value;
        }

        private boolean hasPrev() {
            return prev != null;
        }

        private boolean hasNext() {
            return next != null;
        }

        @Override
        public String toString() {
            return key + " --> " + value;
        }
    }

}
