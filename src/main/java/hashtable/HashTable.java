package hashtable;

import java.util.function.Consumer;

public class HashTable<K, V> {

    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;
    private static final int DEFAULT_CAPACITY = 5;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * Chains are used as collision protection.
     */
    private Chain<K, V>[] chains;
    private float loadFactor;
    private int treshold;
    private int size;

    public HashTable() {
        this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    public HashTable(int capacity) {
        this(capacity, DEFAULT_LOAD_FACTOR);
    }

    @SuppressWarnings("unchecked, WeakerAccess")
    public HashTable(int capacity, float loadFactor) {
        capacity = (capacity << 1) + 1;
        chains = new Chain[capacity];
        for (int i = 0; i < capacity; i++) {
            chains[i] = new Chain<>();
        }
        this.loadFactor = loadFactor;
        changeTreshold();
    }

    private void changeTreshold() {
        treshold = (int) (chains.length * loadFactor);
    }

    public void put(K key, V value) {
        if (size >= treshold) rebuild();
        chains[indexOf(key)].add(key, value);
        size++;
    }

    @SuppressWarnings("unchecked")
    private void rebuild() {
        var old = chains;
        var newCapacity = (old.length << 1) + 1;
        if (newCapacity - MAX_ARRAY_SIZE > 0) {
            if (old.length == MAX_ARRAY_SIZE) return;
            newCapacity = MAX_ARRAY_SIZE;
        }

        chains = new Chain[newCapacity];
        for (int i = 0; i < chains.length; i++) {
            chains[i] = new Chain<>();
        }
        for (Chain<K, V> chain : old) {
            chain.forEachElement((element -> chains[indexOf(element.key)].addUniq(element)));
        }
        changeTreshold();
    }

    private int indexOf(K key) {
        return Math.abs(key.hashCode() % chains.length);
    }

    public V get(K key) {
        return chains[indexOf(key)].get(key);
    }

    public boolean remove(K key) {
        boolean modified = chains[indexOf(key)].remove(key);
        if (modified) size--;

        return modified;
    }

    public void forEach(Consumer<V> action) {
        for (var chain : chains) {
            chain.forEach(action);
        }
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        var stringBuilder = new StringBuilder();
        for (var chain : chains) {
            if (!chain.isEmpty()) stringBuilder.append(chain).append("\n");
        }

        return stringBuilder.toString();
    }

}
