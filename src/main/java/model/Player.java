package model;

/**
 * Imitates Player in some online game.
 */
public class Player {

    private String nickname;
    private int level;

    public Player(String nickname) {
        int nicknameLength = nickname.length();
        if (nicknameLength < 10) {
            throw new IllegalArgumentException("Nickname cannot be less than 20 characters");
        } else if (nicknameLength > 20) {
            throw new IllegalArgumentException("Nickname cannot exceed 20 characters");
        } else this.nickname = nickname;
    }

    public Player(String nickname, int level) {
        this(nickname);
        if (level < 0) {
            throw new IllegalArgumentException("Level can not be lower than 0");
        } else if (level > 99) {
            throw new IllegalArgumentException("Level can not be bigger than 99");
        } else this.level = level;
    }

    public String getNickname() {
        return nickname;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void levelUp() {
        level++;
    }

    @Override
    public String toString() {
        return nickname + " " + level;
    }

}
